#include "dynamic_joint/CreateJoint.hh"


using namespace gazebo;



bool CreateJoint::isAttachedCallback(dynamic_joint::attached_srv::Request& req, dynamic_joint::attached_srv::Response& res)
{
  res.attached_model = model2;
}

void CreateJoint::bumperCallback(const gazebo_msgs::ContactsState& msg) {
  
  gazebo_msgs::ContactState states[1];

  int i = 0;
  
 if(msg.states.empty()) {
   collide = false;
 }
 else { 
   collide=true;
 }
/*  
  for(std::vector<gazebo_msgs::ContactState>::const_iterator it = msg.states.begin(); it != msg.states.end(); ++it) {
    states[i] = *it;
    std::string name = states[i].collision2_name;
    std::cout << "COLLISION OBJECT NAME: " << i << "   ---> "  << name << std::endl;
    i++; 
}
  
 */ 
}


void CreateJoint::bumperCallback2(const gazebo_msgs::ContactsState& msg) {
  
  gazebo_msgs::ContactState states[1];

  int i = 0;
  
 if(msg.states.empty()) {
   collide2 = false;
 }
 else { 
   collide2=true;
 }
/*  
  for(std::vector<gazebo_msgs::ContactState>::const_iterator it = msg.states.begin(); it != msg.states.end(); ++it) {
    states[i] = *it;
    std::string name = states[i].collision2_name;
    std::cout << "COLLISION OBJECT NAME: " << i << "   ---> "  << name << std::endl;
    i++; 
}
  
 */ 
}


bool CreateJoint::attachCallback(dynamic_joint::attach::Request& req, dynamic_joint::attach::Response& res) {
  
	  model1 = req.model1;
	  link1 = req.link1;
	  model2 = req.model2;
	  link2 = req.link2;
	  
	  
	  std::cout << model1 << " " << link1 << " " << model2 << " " << link2 << std::endl;
	  
		this->object1 = world->GetModel(model1);
		this->object2 = world->GetModel(model2);
		/*
		this->engine = world->GetPhysicsEngine();
		this->prevWallTime = common::Time::GetWallTime();
		this->flagMin = false;
		this->flagMax = false;
		*/
	  
	  bool grasp = req.grasp;
	  if(grasp) 
	    CreateDynamicJoint();
	  else DetachJoint();
	  
	  res.success = true;
  
}

bool CreateJoint::Collide(dynamic_joint::collision::Request& req,dynamic_joint::collision::Response& res) {
  res.collision = collide; 
}


bool CreateJoint::Collide2(dynamic_joint::collision::Request& req,dynamic_joint::collision::Response& res) {
  res.collision = collide2; 
}





//////////////////////////////////////////////////
void CreateJoint::Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/) {
  
    int argc = 0;
	  char** argv = NULL;
	  ros::init(argc, argv, "Dynamic_joint_plugin");
	  
	  ROS_INFO("********DYNAMIC JOINT PLUGIN STARTED*********");
	  
	  collide = false;
	  
	 // this->ros_subscriber = n.subscribe("/attach", 1000, &CreateJoint::attachCallback, this);
	  service = n.advertiseService("/attached", &CreateJoint::isAttachedCallback, this);
	  service2 = n.advertiseService("/attach", &CreateJoint::attachCallback, this);
	  ros_subscriber = n.subscribe("/bumper", 1000, &CreateJoint::bumperCallback, this);
	  ros_subscriber2 = n.subscribe("/bumper2", 1000, &CreateJoint::bumperCallback2, this);
	  collision_srv = n.advertiseService("/collide", &CreateJoint::Collide, this);
	  collision_srv2 = n.advertiseService("/collide2", &CreateJoint::Collide2, this);

	  
  
  	  model1 = "a_robot_big";
	  link1 = "a_robot_big::gripper_big_mov::palm";
	  model2 = "no";
	  link2 = "";
	  
  /*
	  model1 = "init__0";
	  link1 = "init__0::SDIC_Gripper::palm";
	  model2 = "no";
	  link2 = "";

	  */
		std::cout << "CreateJoint: Entering Load()" << std::endl;
		this->world =_parent;
		std::cout << world->GetName() << std::endl;
		
		
		//this->object1 = world->GetModel(model1);
		//this->object2 = world->GetModel(model2);
		this->engine = world->GetPhysicsEngine();
		this->prevWallTime = common::Time::GetWallTime();
		this->flagMin = false;
		this->flagMax = false;
		
		
	}
/*	
void CreateJoint::attachCallback(const dynamic_joint::attached& msg) {
  
  std::cout << "Entra callback" << std::endl;
  
    	  model1 = msg.model1;
	  link1 = msg.link1;
	  model2 = msg.model2;
	  link2 = msg.link2;
	  
	  std::cout << model1 << std::endl;
	  std::cout << link1 << std::endl;
	  std::cout << model2 << std::endl;
	  std::cout << link2 << std::endl;
	  
	  
		this->object1 = world->GetModel(model1);
		this->object2 = world->GetModel(model2);
		this->engine = world->GetPhysicsEngine();
		this->prevWallTime = common::Time::GetWallTime();
		this->flagMin = false;
		this->flagMax = false;
		
	  
	  bool grasp = msg.grasp;
	  if(grasp) 
	    CreateDynamicJoint();
	  else DetachJoint();
}

*/

	


//////////////////////////////////////////////////
void CreateJoint::Init() {
	std::cout << "CreateJoint: Entering Init()" << std::endl;
	this->loadWorld = event::Events::ConnectWorldUpdateBegin(boost::bind(&CreateJoint::OnUpdate, this));
	std::cout << "Create dynamic joint" << std::endl;
	//CreateDynamicJoint();

	
	}
	

	//void CreateJoint::CreateDynamicJoint(physics::LinkPtr _parent_link, physics::LinkPtr _child_link)
void CreateJoint::CreateDynamicJoint()
{

		math::Vector3 axis,direction;

	      axis = direction.Cross(math::Vector3(0.0, 0.0, 0.0));
		
			this->myJoint = this->engine->CreateJoint("revolute", this->object1);
			this->jointPose = math::Pose(0, 0, 0, 0, 0, 0);
			this->myJoint->Load(this->object1->GetLink(link1),  this->object2->GetLink(link2), this->jointPose);
			this->myJoint->Attach(this->object1->GetLink(link1), this->object2->GetLink(link2));
			std::cout << "Joint attached successfully!" << std::endl;
			std::cout << "Connected " << link1 << " with model " << model2 << " and link " << link2 << std::endl;

			//this->flagMin = true;
			 
			this->myJoint->SetAxis(0, axis);
			this->myJoint->SetHighStop(0,0.0);
			this->myJoint->SetLowStop(0,0.0);
						
			//bool connected = this->myJoint->AreConnected(this->object1->GetLink(link1), this->object2->GetLink(link2));
			//std::cout << "CONNECTED: " << connected << std::endl;
			std::cout << "Connected " << link1 << " with model " << model2 << " and link " << link2 << std::endl;


        }
        
        void CreateJoint::DetachJoint() {
	  
	this->myJoint->Detach();
	std::cout << "Joint detached!" << std::endl;
	this->flagMax = true;
	model2= "no";
	link2= "";
	
	}



//////////////////////////////////////////////////
void CreateJoint::OnUpdate() {



	}

GZ_REGISTER_WORLD_PLUGIN(CreateJoint)