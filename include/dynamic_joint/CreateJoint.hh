#ifndef _CREATE_JOINT_HH
#define _CREATE_JOIN_HH

#include "gazebo/gazebo.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include <ros/ros.h>
#include <dynamic_joint/attached.h>
#include <dynamic_joint/attached_srv.h>
#include <dynamic_joint/attach.h>
#include "gazebo_msgs/ContactsState.h"
#include "gazebo_msgs/ContactState.h"
#include <dynamic_joint/collision.h>


namespace gazebo {
/// \Class CreateJoint create_joint.hh
	class CreateJoint: public WorldPlugin

	{
	  
	      
	      public: void bumperCallback(const gazebo_msgs::ContactsState& msg);
	      public: void bumperCallback2(const gazebo_msgs::ContactsState& msg);
	  
	  
	      public: bool isAttachedCallback(dynamic_joint::attached_srv::Request &req, 
					      dynamic_joint::attached_srv::Response &res );
	      
	      public: bool attachCallback(dynamic_joint::attach::Request& req, 
							dynamic_joint::attach::Response& res);
	      
	      public: bool Collide(dynamic_joint::collision::Request& req, 
				   dynamic_joint::collision::Response& res);
	      
	      public: bool Collide2(dynamic_joint::collision::Request& req, 
				   dynamic_joint::collision::Response& res);

		/// \brief Load.
		public: void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/);

		/// \brief Init.
		public: void Init();

		/// \brief OnUpdate.
		public: void OnUpdate();

		public: void CreateDynamicJoint();
		
		public: void DetachJoint();

		/// \brief Standard physics world.
		public: physics::WorldPtr world;
		
		/// \brief Standard physics onject1.
		public: physics::ModelPtr object1;
		
		/// \brief Standard physics onject2.
		public:	physics::ModelPtr object2;
		
		/// \brief Standard connection load World.
		public:	event::ConnectionPtr loadWorld;
		
		/// \brief Standard physics myJoint.
		public:	physics::JointPtr myJoint;
		
		/// \brief Standard physics engine.
		public:	physics::PhysicsEnginePtr engine;
		
		/// \brief Standard math pose.
		public:	math::Pose jointPose;
		
		/// \brief A static zero time variable set to common::Time(0, 0).
		public:	common::Time prevWallTime;
		
		/// \brief Min value flag
		public:	bool flagMin;

		/// \brief Max value flag
		public:	bool flagMax;
		
	private:
	  
	  ros::NodeHandle n;
	  ros::Subscriber ros_subscriber;
	  ros::Subscriber ros_subscriber2;
	  ros::ServiceServer service;
	  ros::ServiceServer service2;
	  ros::ServiceServer collision_srv;
	  	  ros::ServiceServer collision_srv2;

	  std::string model1;
	  std::string model2;
	  std::string link1;
	  std::string link2;
	  bool collide;
	  bool collide2;

 };
}
#endif
